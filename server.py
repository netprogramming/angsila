import socket

def main():
    server_host = "0.0.0.0"  # เปลี่ยนเป็น IP Address ของ server
    server_port = 12345

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server_socket.bind((server_host, server_port))

    server_socket.listen(1)
    print("Server is listening on {}:{}".format(server_host, server_port))

    while True:
        print("Waiting for a connection...")
        client_socket, client_address = server_socket.accept()
        print("Accepted connection from {}:{}".format(client_address[0], client_address[1]))

        try:
            with open("a.txt", "rb") as file:
                file_contents = file.read()
                client_socket.sendall(file_contents)
            print("File contents sent to client.")
        except Exception as e:
            print("Error sending file:", e)
        finally:
            client_socket.close()

if __name__ == "__main__":
    main()
