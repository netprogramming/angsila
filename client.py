import socket

def main():
    server_host = "10.80.4.16"  # เปลี่ยนเป็น IP Address ของ server
    server_port = 12345

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        client_socket.connect((server_host, server_port))
        print("Connected to {}:{}".format(server_host, server_port))

        file_contents = client_socket.recv(4096)
        print("Received file contents:")
        print(file_contents.decode("utf-8"))
    except Exception as e:
        print("Error:", e)
    finally:
        client_socket.close()

if __name__ == "__main__":
    main()
